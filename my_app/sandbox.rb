require "lucid_http"

GET "/posts/1234/show"
body                            # => "You accessed the <show> action for post number 1234"
status                          # => 200 OK

GET "/posts/my_post/show"
body                            # => ""
status                          # => 404 Not Found

GET "/posts/a7/show"
body                            # => ""
status                          # => 404 Not Found
