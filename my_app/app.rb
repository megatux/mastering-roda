class App < Roda
  route do |r|
    r.is "posts", Integer, :action do |id, action|
      "You accessed the <#{action}> action for post number #{id}"
    end
  end
end
